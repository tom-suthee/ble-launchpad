# system
import sys
import signal

# BLE
from pybleno import *
from MidiService import *

# for launchpad
import launchpad_py as lp
from LaunchpadState import *

# helpers from pygame
from pygame import time


bleno = Bleno()
midiService = MidiService()
serviceName = 'BLE Midi'
running = True
launchpadState = None

def onStateChange(state):
    global launchpadState
    if (state == 'poweredOn'):
        bleno.startAdvertising(serviceName, [midiService.uuid])
    else:
        bleno.stopAdvertising()
bleno.on('stateChange', onStateChange)

def onAdvertisingStart(error):
    global launchPadState, running
    
    if not error:
        def onSetServiceError(error):
            if not error:
                launchPadState.bleAdvertised()
            else:
                print('setServices: %s' % error)
                launchPadState.bleError()
                running = False
                
        bleno.setServices([
            midiService
        ], onSetServiceError)
    else:
        print('on -> advertisingStart: %s' % error)
bleno.on('advertisingStart', onAdvertisingStart)

def signal_handler(sig, frame):
    global running
    running = False	
signal.signal(signal.SIGINT, signal_handler)

def main():
    global running, bleno, launchPadState

    # Init launchpad
    launchpad = lp.LaunchpadMk2()
    launchPadState = LaunchpadState(launchpad)
    midiService.setLaunchpadState(launchPadState)

    if launchpad.Open( 0, "mk2" ):
        launchPadState.launchpadConnected()
    else:
        print(" - Launchpad Mk2: ERROR")
        sys.exit(1)

    launchPadState.reset()
    launchPadState.notePadCheckUp()

    # start BLE
    bleno.start()
    
    while running:
        state = launchpad.ButtonStateRaw()
        if len(state) > 0:
            launchPadState.press(state)
        else:
            time.wait(10)

    # stop launchpad
    launchpad.Reset()
    launchpad.Close()
    
    # stop BLE
    bleno.stopAdvertising()
    bleno.disconnect()

    print('\nGoodbye')
    sys.exit(1)

if __name__ == '__main__':
    main()
