def extend(obj, array):
    tmp = obj if isinstance(obj, list) else [obj]
    tmp.extend(array)
    return tmp
