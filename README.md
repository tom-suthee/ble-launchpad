# BLE Launchpad in Python

This project aims to combine BLE with Launchpad.
These powerfull projects [pybleno](https://github.com/Adam-Langley/pybleno) and [launchpad.py](https://github.com/FMMT666/launchpad.py), make thing lots easier!.

My Project is develop on Raspberry Pi 3B and Python2.7, however it should runable on any BLE support devices.

# Installation
## BLE Preparation
#### Install bluez
```sh
sudo apt-get install bluetooth bluez libbluetooth-dev libudev-dev
```

#### Stop bluetoothd
```sh
sudo systemctl stop bluetooth
sudo systemctl disable bluetooth
```

#### Start bluez
```sh
sudo hciconfig hci0 up
```
## Python Preparation
#### PIP
```sh
sudo apt install python-pip
```

### SDL
```sh
sudo apt install libsdl1.2-dev
```

#### pybleno
```sh
sudo pip install git+https://github.com/Adam-Langley/pybleno.git
```
> The PIP package version 0.11 has some issue, so we've to install from Github directly

#### Launchpad
```sh
sudo pip install launchpad-py numpy pygame
```

# How to use
Simply run
```sh
sudo python main.py
```
And the the checkup light should be display on Launchpad.

I've customized the Launchpad to work in 2 modes.
- Pad mode
where only the white pad buttons will br send through BLE. The pad will be start from C1 (bottm-left), C#1, D1 and so on.
You can shift the note of start pad up to C2 by pressing "up arrow", and shift the note down by pressing "down arrow".
Pressing "left arrow" will show the current start note. You can change from C-2 to C7.
- Raw mode
This mode will all the data when you press the button through BLE and process the received data.

To toggle between these modes,
- Press "Session" + "User 1" to change to Raw mode
- Press "Session" + "User 2" to change to Pad mode
Also, when in Pad mode, press "Session" + "Mixer" will toggle the note guid color.

Press "Ctrl" + "C" from terminal to quit the process.

## Licenses and Attribution

### Adam Langley's Pybleno
https://github.com/Adam-Langley/pybleno

### launchpad.py
https://github.com/FMMT666/launchpad.py

### PyGame
https://github.com/pygame/pygame

### Sandeep Mistry's Bleno
https://github.com/sandeepmistry/bleno

### Wayne Keenan's BluetoothHCI
https://github.com/TheBubbleworks/python-hcipy

### Mike Ryan's BluetoothSocket taken from Python control for Leviton Decora Bluetooth switches
https://github.com/mjg59/python-decora
