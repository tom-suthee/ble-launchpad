import array


class MidiDescriptor:
    def __init__(self, options):
        self._events = {}
        self.uuid = options['uuid']
        if isinstance(options['value'], str):
            self.value = array.array('B', [ord(c) for c in options['value']])
        else:
            self.value = options['value']


    def on(self, event, handler):
        print 'on', event
        self._events = self._events
        handlers = self._events[event] = self._events[event] if event in self._events else []
        handlers.append(handler)


    def off(self, event, handler):
        print 'off', event
        handlers = self._events[event] = self._events[event] if event in self._events else []
        handlers.remove(handler)


    def emit(self, event, arguments):
        print 'emit', event, arguments
        # print self._events
        # print self._events[event]
        handlers = self._events[event] if event in self._events else []
        for handler in handlers:
            handler(*arguments)


    def once(self, event, arguments, handler):
        print 'once', event, arguments
        def temporary_handler(*arguments):
            self.off(event, temporary_handler)
            handler(*arguments)

        self.on(event, temporary_handler)
